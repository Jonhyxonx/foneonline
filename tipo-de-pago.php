<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>FONEONLINE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/fontawesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="style.css" rel="stylesheet"/>
        <link rel="shortcut icon" href="assets/ico/logo0.png">
        <style>
            .fondo{;
                    height: 50px;
                    width: 80px;
                    opacity: 0.95;
                    padding: 5px 0px;
                    margin: 2px 0px 0px;}
            .lateral {position: relative;
                    left: 75px;
                	border-radius: 8px}
            .borde{ border-radius: 8px }

        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="login.php"><img src="imgs/logo0.png" width="30%"></a>
       
        </nav>
        <header id="home">
            <div class="content">
                <div class="d-flex align-items-center flex-column justify-content-center">
                    <h1>Elige tu método de pago!</h1> 
                    <div class="fondo" >
                            <a href="x/pago.html"><img  src="imgs/mc.png"></a>
                            <ul></ul> 
                            <a  href="x/paypalpago.html"><img src="imgs/pp.png"></a>
                            <ul></ul>
                        <img src="imgs/visa.png">
                            <ul></ul>                            
                    </div>
                       <!--<form action="includes/log.php" method="POST" class="fondo" >
                            <ul></ul>
                                 <h4 class="text-center">Inicia Sesión</h4>
                            <ul></ul>
                                <input type="text" placeholder="Nombre de Usuario" name="usuario" class="borde" required>
                            <ul></ul>
                                <input type="password" placeholder="Ingrese su contraseña" name="contrasena" class="borde" required>
                            <ul></ul> 
                            <input type="submit" value="Iniciar" name="iniciar" class="btn">                            
                        </div>-->
                    </div>
                </div>
            </div>
            <!--<div id="slideToNext" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                         <img src="imgs/fondo2.png" class="d-block w-100">
                    </div>
                    <div class="carousel-item">
                        <img src="imgs/fondo3.png" class="d-block w-100">
                    </div>
                </div>
          </div>-->
    </header>
    <footer>
        <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-12">
                        <a href="login.php" class="logo"><img src="imgs/logo0.png" alt="#" width="30%" /></a>
                    </div>
                    <div class="col-lg-9 col-12">
                        <div class="row">
                            <div class="col-md-4 col-12">
                                <i class="fas fa-map-marker-alt fa-2x"></i>
                                <span>Gb road Coronel Bogado PY </span>
                            </div>
                            <div class="col-md-4 col-12">
                                <i class="fas fa-phone fa-2x"></i>
                                <span>(+595)984354294</span>
                            </div>
                            <div class="col-md-4 col-12">
                                <i class="fas fa-envelope fa-2x"></i>
                                <span>foneonline@gmail.com</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="item">
                            <h4 class="text-capitalize">Fone Online</h4>
                            <p>Fone Online es una compañia dedicada a la venta de smartphones y sus accesorios totalmente online.Desde el 2012 empezamos este proyecto de llevar a los hogares de nuestros clientes los mejores equipos moviles.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="item">
                            <h4 class="text-capitalize">Menu</h4>
                            <ul class='text-capitalize main-list'>
                                <li><a href="#home">home</a></li>
                                <li><a href="#about">about us</a></li>
                                <li><a href="#service">Services</a></li>
                                <li><a href="#test">testimonial</a></li>
                                <li><a href="#contact">contact us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="item">
                            <h4 class="text-capitalize">Our network</h4>
                            <ul class="text-capitalize">
                                <li><a href="#">Adopt a pet</a></li>
                                <li><a href="#">create awareness</a></li>
                                <li><a href="#">join compaign</a></li>
                            </ul>
                        </div>
                    </div>
                   
                <ul class="social-media">
                    <li><a href="#"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter-square fa-2x"></i></a></li>
                    <li><a href="#"><i class="fab fa-google-plus-square fa-2x"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin fa-2x"></i></a></li>
                </ul>
                    <p>© 2018 All Rights Reserved. Free Website Templates</p>
            </div>
    </footer>    
            
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(function () {
                
                'use strict';
                
                var winH = $(window).height();
                
                $('header, .slide').height(winH);
                
                $('.navbar a.search-link').on('click', function () {
                    $(this).siblings('form').fadeToggle();
                });
                
                $('.navbar ul.navbar-nav li a, footer ul.main-list li a').on('click', function (e) {
                    
                    var getAttr = $(this).attr('href');
                    
                    e.preventDefault();
                    $('html').animate({scrollTop: $(getAttr).offset().top}, 1000);
                });
            });
        </script>
    </body>
</html>