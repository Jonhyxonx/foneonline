<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>FONEONLINE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <!-- Customize styles -->
    <link href="style.css" rel="stylesheet"/>
    <!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<style>
		.fondo{background-color: black;
				height: 190px;
				width: 230px;
				opacity: 0.8}
		.lateral {position: relative;
				right: 20px;
				border-radius: 8px}

	</style>
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
  </head>
<body>
<!-- 
	Upper Header Section 
-->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="topNav">
		<div class="container">
			<div class="alignR">
				
				
			</div>
		</div>
	</div>
</div>

<!--
Lower Header Section 
-->
<div class="container">
<div id="gototop"> </div>
<header id="header">
<div class="row">
	<div class="span4">
	<h1>
	<a class="logo" href="index.html"><span>FONEONLINE</span> 
		<img src="imgs/logo0.png" width="60%" alt="#">
	</a>
	</h1>
	</div>

	
	
</div>
</header>

<!--
Navigation Bar Section 
-->
<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
		  <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </a>
		  <div class="nav-collapse">
			<ul class="nav">
			  <li class="active"><a href="index.php">Inicio	</a></li>
			  <li class=""><a href="list-view.php">Todos los productos</a></li>
			</ul>
			
			<ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span> Login <b class="caret"></b></a>
				<div class="dropdown-menu">
				<form class="form-horizontal loginFrm">
				  <div class="control-group">
					<input type="text" class="span2"  placeholder="Usuario">
				  </div>
				  <div class="control-group">
					<input type="password" class="span2" placeholder="Contraseña">
				  </div>
				  <div class="control-group">
					<label class="checkbox">
					<a class="lateral"> Registrarse</a>
					</label>
					<button type="submit" class="shopBtn btn-block">Iniciar Sesión</button>
				  </div>
				</form>
				</div>
			</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
<!-- 
Body Section 
-->
	<div class="row">
<div id="sidebar" class="span3">
<div class="well well-small">
	<ul class="nav nav-list">
		<li><a href="products.html"><span class="icon-chevron-right"></span>SAMSUNG</a></li>
		<li><a href="products.html"><span class="icon-chevron-right"></span>APPLE</a></li>
		<li><a href="products.html"><span class="icon-chevron-right"></span>HUAWEY</a></li>
		<li><a href="products.html"><span class="icon-chevron-right"></span>XIAOMI</a></li>
		<li><a href="products.html"><span class="icon-chevron-right"></span>MOTOROLA</a></li>
		<li><a href="products.html"><span class="icon-chevron-right"></span>ALCATEL</a></li>
	</ul>
</div>

			  
			<br>
			<br>
			<ul class="nav nav-list promowrapper">
			<li>
			  
			</li>
			<li style="border:0"> &nbsp;</li>
			<li>
			  
			</li>
			<li style="border:0"> &nbsp;</li>
			
		  </ul>

	</div>
	<div class="span9">
    <ul class="breadcrumb">
    <li><a href="index.php">Inicio</a> <span class="divider">/</span></li>
    <li><a href="products.php">Todos los productos</a> <span class="divider">/</span></li>
    <li class="active">Ver mas</li>
    </ul>	
	<div class="well well-small">
	<div class="row-fluid">
			<div class="span5">
			<div id="myCarousel" class="carousel slide cntr">
                <div class="carousel-inner">
                  <div class="item active">
                   <a href="#"> <img src="assets/img/a.jpg" alt="" style="width:100%"></a>
                  </div>
                  <div class="item">
                     <a href="#"> <img src="assets/img/b.jpg" alt="" style="width:100%"></a>
                  </div>
                  <div class="item">
                    <a href="#"> <img src="assets/img/e.jpg" alt="" style="width:100%"></a>
                  </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
            </div>
			</div>
			<div class="span7">
				<h3>Nombre del producto</h3>
				<hr class="soft"/>
				
				<form class="form-horizontal qtyFrm">
				  <div class="control-group">
					<label class="control-label"><span>Aca debe ir el precio</span></label>
					<div class="controls">>
					</div>
				  </div>
				
				  <div class="control-group">
					<label class="control-label"><span>Aca debe ir la marca</span></label>
					<div class="controls">
					</div>
				  </div>
				  <div class="control-group">
					<label class="control-label"><span>Aca debe ir el modelo</span></label>
				  </div>
				  <button type="submit" class="shopBtn"><span class=" icon-shopping-cart"></span> Agregar al carrito</button>
				</form>
			</div>
			</div>
				<hr class="softn clr"/>


<div class="copyright">

</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
  </body>
</html>