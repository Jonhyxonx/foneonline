<?php

class DB{
	
	private $host;
	private $db;
	private $username;
	private $password;
	//private $charset;

	public function __constructor(){
		$this->host = 'localhost';
		$this->db = 'proyectofinal';
		$this->username = 'root';
		$this->password ="root";
		//$this->charset = 'utf8mb4';
	}

	function connect(){
		
		try{
			$connection = "mysql:host=" . $this->host . ";dbname=" . $this->db;
			$options = [
				PDO::ATTR_ERRMODE			=> PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES	=> false,
			];
			$pdo = new PDO($connection, $this->user, $this->password, $options);
			
			return $pdo;
			
			
		}catch(PDOException $e){
			die("Error connection: " . $e->getMessage());
		}
	}
}

?>