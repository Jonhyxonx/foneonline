<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>FONEONLINE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet"/>
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link rel="shortcut icon" href="assets/ico/logo0.png">
	<style>
		.fondo{background-color: black;
				height: 190px;
				width: 230px;
				opacity: 0.8}
		.lateral {position: relative;
				right: 20px;
				border-radius: 8px}
		.table-dark{background-color: black;
					color: white;}
		.table-light{
			background-color: white;
					color: black;
					border: 0px;
					
		}
		.text_center{
			text-align: left;
		}
		.width{
			width: 150px;
		}
		.height{
			height: 190px;
		}
		}
	</style>
  </head>
<body>
<div class="container">
<div id="gototop"> </div>
<header id="header">
<div class="row">
	<div class="span4">
	<h1>
	<a class="logo" href="index.php"><span>Twitter Bootstrap ecommerce template</span> 
		<img src="imgs/logo0.png" width="60%" alt="#" >
	</a>
	</h1>
	</div>
</header>
<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
		  <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </a>
		  <div class="nav-collapse">
			<ul class="nav">
			  <li class=""><a href="index.php">Inicio	</a></li>
			  <li class="active"><a href="list-view.php">Todos los productos</a></li>
			</ul>
			<ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span> Login <b class="caret"></b></a>
				<div class="dropdown-menu">
				<form class="form-horizontal loginFrm" action="includes/log.php" method="POST">
						<div class="control-group" >
							<input type="text" class="span2" name="usuario"  placeholder="Usuario">
						</div>
						<div class="control-group" >
							<input type="password" class="span2" name="contrasena" placeholder="Contraseña">
						</div>
						<div class="control-group">
							<label class="checkbox">
							<a class="lateral" href="login.php"> Registrarse</a>
							</label>
							<button type="submit" class="shopBtn btn-block">Iniciar Sesión</button>
						</div>
					</form>
				</div>
			</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
<!-- 
Body Section 
-->
	<div class="row">
<div id="sidebar" class="span3">
<div class="well well-small">
	<ul class="nav nav-list">
		<li><a href="list-view-samsung.php"><span class="icon-chevron-right"></span>SAMSUNG</a></li>
		<li><a href="list-view-huawei.php"><span class="icon-chevron-right"></span>HUAWEI</a></li>
		<li><a href="list-view-xiaomi.php"><span class="icon-chevron-right"></span>XIAOMI</a></li>
		<li style="border:0"> &nbsp;</li>
		<!--<li> <a class="totalInCart" href="cart.html"><strong>Total Amount  <span class="badge badge-warning pull-right" style="line-height:18px;">$448.42</span></strong></a></li>-->
	</ul>
</div>

			  
			<br>
			<br>
			<ul class="nav nav-list promowrapper">
			<li style="border:0"> &nbsp;</li>

		  </ul>

	</div>
<div class="span9">
<table class="table">
		<thead class="table-dark">
		<tr>
			<td>Marca</td>
			<td>Photo</td>
			<td class="width">Modelo/Descripción</td>
			<td>Precio</td>
		</tr>
		</thead>
		<?php 
			require 'includes/DB.php';
			$show = "SELECT m.nombre, p.imagen, p.modelo, p.precio FROM producto p inner join marca m WHERE m.idmarca=p.idmarca and m.idmarca='4'";
			$consulta = mysqli_query($conexion,$show);
			
			while($mostrar=mysqli_fetch_array($consulta)){
				if($mostrar['imagen']!="NULL"){
					$imagen = 'imgs2/'.$mostrar['imagen'];
				}else{
					$imagen = 'imgs/pic8.jpg'.$mostrar['imagen'];
				}
			?>
		<tbody class="table-light">
		<tr>
			<td class="height"><?php echo $mostrar['nombre']?></td>
			<td><img src="<?php echo $imagen;?>"  alt="#"></td>
			<td class="width"><p class="text-left"><?php echo $mostrar['modelo']?></p></td>
			<td><?php echo $mostrar['precio']?></td>
		</tr>
		<tbody>
		<?php 
		}
		?>
</table>

</div>
<!-- 
Clients 
-->
<section class="our_client">
	<hr class="soften"/>
	
	<hr class="soften"/>
	
</section>
<div class="copyright">
<div class="container">	
	<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
</div>
</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
  </body>
</html>