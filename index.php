<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>FONEONLINE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <link href="style.css" rel="stylesheet"/>
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">	
	<link rel="shortcut icon" href="assets/ico/logo0.png">
	<style>
		.fondo{background-color: black;
				height: 190px;
				width: 230px;
				opacity: 0.8}
		.lateral {position: relative;
				right: 20px;
				border-radius: 8px}

	</style>
  </head>
<body>
<div class="container">
<div id="gototop"> </div>
<header id="header">
<div class="row">
	<div class="span4">
	<h1>
	<a class="logo" href="index.php"><span>FONEONLINES</span> 
		<img src="imgs/logo0.png" width="60%">
	</a>
	</h1>
</header>

<div class="navbar">
	  <div class="navbar-inner">
		<div class="container">
		  <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </a>
		  <div class="nav-collapse">
			<ul class="nav">
			  <li class="active"><a href="index.php">Inicio	</a></li>
			  <li class=""><a href="list-view.php">Todos los productos</a></li>
			 
			</ul>
			
			<ul class="nav pull-right">
			<li class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="icon-lock"></span> Login <b class="caret"></b></a>
				<div class="dropdown-menu">
					<form class="form-horizontal loginFrm" action="includes/log.php" method="POST">
					<?php
						if(isset($errorlogin)){
							echo $errorlogin;
						}
						?>
						<div class="control-group" >
						
							<input type="text" class="span2" name="usuario"  placeholder="Usuario">
						</div>
						<div class="control-group" >
							<input type="password" class="span2" name="contrasena" placeholder="Contraseña">
						</div>
						<div class="control-group">
							<label class="checkbox">
							<a class="lateral" href="login.php"> Registrarse</a>
							</label>
							<button type="submit" class="shopBtn btn-block">Iniciar Sesión</button>
						</div>
					</form>
				</div>
			</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
	<div class="row">
<div id="sidebar" class="span3">
<div class="well well-small">
	<ul class="nav nav-list">
		<li><a href="list-view-samsung.php"><span class="icon-chevron-right"></span>SAMSUNG</a></li>
		<li><a href="list-view-apple.php"><span class="icon-chevron-right"></span>APPLE</a></li>
		<li><a href="list-view-huawei.php"><span class="icon-chevron-right"></span>HUAWEI</a></li>
		<li><a href="list-view-xiaomi.php"><span class="icon-chevron-right"></span>XIAOMI</a></li>
		<li style="border:0"> &nbsp;</li>
	</ul>
</div>
	</div >
	<div class="span9">
	<div class="well np">
		<div id="myCarousel" class="carousel slide homCar">
            <div class="carousel-inner">
			  <div class="item">
                <img style="width:100%" src="assets/img/1100.jpg"  alt="bootstrap ecommerce templates">
                <div class="carousel-caption">
                      <h4>Descubre los mejores productos</h4>
                      <p><span>Con los mejores precios del mercado</span></p>
                </div>
              </div>
			  <div class="item">
                <img style="width:100%" src="assets/img/9pro.png" alt="bootstrap ecommerce templates">
                <div class="carousel-caption">
                      <h4>Celulares de ultima generacion</h4>
                      <p><span>Lo ultimo del nuevo mercado 2020</span></p>
                </div>
              </div>
			  <div class="item active">
                <img style="width:100%" src="assets/img/12pro.jpg" alt="bootstrap ecommerce templates">
                <div class="carousel-caption">
                      <h4>Garantia total                  </h4>
                      <p><span>Sello de calidad</span></p>
                </div>
              </div>
              <div class="item">
                <img style="width:100%" src="assets/img/celu.jpeg" alt="bootstrap templates">
                <div class="carousel-caption">
                      <h4>Empresa lider en venta de celulares</h4>
                      <p><span>Con sucursales en todo el pais</span></p>
                </div>
              </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
          </div>
        </div>
	<div class="well well-small">
	<h3>Nuevos Productos </h3>
	<hr class="soften"/>
		<div class="row-fluid">
		<div id="newProductCar" class="carousel slide">
            <div class="carousel-inner">
			<div class="item active">
			  </div>
		   <div class="item">
		  </div>
		   </div>
		  <a class="left carousel-control" href="#newProductCar" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#newProductCar" data-slide="next">&rsaquo;</a>
		  </div>
		  </div>
		  <div class="row-fluid">
		  <ul class="thumbnails">
			<li class="span4">
			  <div class="thumbnail">
				 
				<a class="zoomTool" href="list-view.php" title="AGREGAR"><span class="icon-search"></span> VER MAS</a>
				<a href="list-view.php"><img src="imgs/celular-xiaomi-redmi-note-8-64-gb.jpg" alt=""></a>
				<div class="caption cntr">
					<p>xiaomi redmi note 8 64GB</p>
					<p><strong>1.750.000 Gs</strong></p>
					<h4><a class="shopBtn" href="" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </form>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="list-view.php" title="AGREGAR"><span class="icon-search"></span>VER MAS</a>
				<a href="list-view.php"><img src="imgs/celular-xiaomi-note-9-pro-128gb.jpg" alt=""></a>
				<div class="caption cntr">
					<p>xiaomi note 9 pro 128GB</p>
					<p><strong>2.390.000 Gs</strong></p>
					<h4><a class="shopBtn" href="#" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="list-view.php" title="AGREGAR"><span class="icon-search"></span>VER MAS</a>
				<a href="list-view.html"><img src="imgs/celular-huawei-mate-20-128-gb.jpg" alt=""></a>
				<div class="caption cntr">
					<p>huawei mate 20 128GB</p>
					<p><strong>4.100.000 Gs</strong></p>
					<h4><a class="shopBtn" href="#" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
		  </ul>
		</div>
	</div>
		<div class="well well-small">
		  <h3></a> Productos Destacados  </h3>
		  <hr class="soften"/>
		  <div class="row-fluid">
		  <ul class="thumbnails">
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="list-view.php" title="add to cart"><span class="icon-search"></span> VER MAS</a>
				<a  href="list-view.php"><img src="imgs/celular-samsung-galaxy-a51-128-gb.jpg" alt=""></a>
				<div class="caption cntr">
				<p>samsung galaxy a51 128 GB</p>
					<p><strong>2.450.000 Gs</strong></p>
					<h4><a class="shopBtn" href="#" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="list-view.php" title="add to cart"><span class="icon-search"></span> VER MAS</a>
				<a  href="list-view.html"><img src="imgs/celular-iphone-11-pro-max-64gb.jpg" alt=""></a>
				<div class="caption cntr">
				<p>iphone 11 pro max 64GB</p>
					<p><strong>9.700.000 Gs</strong></p>
					<h4><a class="shopBtn" href="#" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
			<li class="span4">
			  <div class="thumbnail">
				<a class="zoomTool" href="list-view.php" title="add to cart"><span class="icon-search"></span> VER MAS</a>
				<a  href="list-view.html"><img src="imgs/celular-iphone-12-pro-max-128gb.jpg" alt=""/></a>
				<div class="caption cntr">
				<p>iphone 12 pro max 128 GB</p>
					<p><strong>13.650.000 Gs</strong></p>
					<h4><a class="shopBtn" href="#" title="AGREGAR"> Agregar al carrito </a></h4>
					<div class="actionList">
						
					</div> 
					<br class="clr">
				</div>
			  </div>
			</li>
		  </ul>	
	</div>
	</div>

<div class="copyright">
<div class="container">
	
	<span>Copyright &copy; 2013<br> bootstrap ecommerce shopping template</span>
</div>
</div>
<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="assets/js/shop.js"></script>
  </body>
</html>